const request = require("request")

const forecast = (latitude, longitude, callback) => {
    const url = "https://api.darksky.net/forecast/b9d36f75b878830087d80ac8919cb355/" + latitude + "," + longitude + ""

    request({url, json: true}, (error, {body}) => {
        if (error) {
            callback("Unable to connect to weather services!!", undefined)
        } else if (body.error) {
            callback("Weather report not found!!", undefined)
        } else {
            callback(undefined, {
                temperature: body.currently.temperature,
                precipProbability: body.currently.precipProbability
            })
        }
    })
}

module.exports = forecast



// calling function is in chat-app/src/index.js