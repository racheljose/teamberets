var Sequelize = require('sequelize');
// export NODE_ENV=development
var bcrypt = require('bcryptjs');
const database = require('../server/config/database');
var conString = database.conString;


// create a sequelize instance with our local postgres database information.
const sequelize = new Sequelize(conString, {
  dialect: 'postgres'
});
// var sequelize = new Sequelize({
//     connectionString: conString,
// }); 

// setup User model and its fields.

    
	/*================================================================
	                            admin
    =================================================================*/
var admin = sequelize.define('admin', {
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
},  {
  hooks: {
    beforeCreate: (user) => {
      const salt = bcrypt.genSaltSync();
      user.password = bcrypt.hashSync(user.password, salt);
    }
  }
});


admin.prototype.validPassword = async function(password) {
  return await bcrypt.compare(password, this.password);
}
admin.prototype.roles = async function() {
  return await this.role;
}


    
	/*================================================================
	                        victim
    =================================================================*/


var victim = sequelize.define('victim', {
    name: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false
    },
    phno: {
        type: Sequelize.INTEGER,
        unique: true,
        allowNull: false
    },
    latitude: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    longitude: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});



    
	/*================================================================
	                        volunteer
    =================================================================*/
    var volunteer = sequelize.define('volunteer', {
        name: {
            type: Sequelize.STRING,
            unique: false,
            allowNull: false
        },
        phno: {
            type: Sequelize.INTEGER,
            unique: true,
            allowNull: false
        },
        latitude: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        longitude: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

sequelize.sync()
    .then(() => console.log('tables has been successfully created, if one doesn\'t exist'))
    .catch(error => console.log('This error occured', error));

// export User model for use in other files.
module.exports = {
    admin,
    victim,
    volunteer
}