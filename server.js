/* ========================================================== 
                        Libraries
============================================================ */
const express = require("express");
const request = require("request");
const path = require("path");
const hbs = require("hbs");
var logger   = require('morgan');								//logger middleware
const bodyParser = require("body-parser");
const http = require("http");
const session = require('express-session');
var cookieParser = require('cookie-parser');
const User = require('./models/user');
const app = express()

const socketio = require('socket.io')
const Filter = require('bad-words')


const { generateMessage, generateLocationMessage } = require('C:/Users/HP/Documents/GitHub/natural_calamities/logis/maps-api-for-javascript-examples-master/map-with-route-from-a-to-b/src/utils/messages')
const { addUser, removeUser, getUser, getUsersInRoom } = require('C:/Users/HP/Documents/GitHub/natural_calamities/logis/maps-api-for-javascript-examples-master/map-with-route-from-a-to-b/src/utils/users')

const io = socketio(server)

const port = process.env.PORT || 3000
const publicDirectoryPath = path.join(__dirname, 'C:/Users/HP/Documents/GitHub/natural_calamities/logis/maps-api-for-javascript-examples-master/map-with-route-from-a-to-b/public')

/*================================================================
                    Socket code
=================================================================*/


io.on('connection', (socket) => {
    console.log('New WebSocket connection')

    socket.on('join', (options, callback) => {
        const { error, user } = addUser({ id: socket.id, ...options })

        if (error) {
            return callback(error)
        }

        socket.join(user.room)

        socket.emit('message', generateMessage('Admin', 'Welcome!'))
        socket.broadcast.to(user.room).emit('message', generateMessage('Admin', `${user.username} has joined!`))
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUsersInRoom(user.room)
        })

        callback()
    })

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id)
        const filter = new Filter()

        if (filter.isProfane(message)) {
            return callback('Profanity is not allowed!')
        }

        io.to(user.room).emit('message', generateMessage(user.username, message))
        callback()
    })

    socket.on('sendLocation', (coords, callback) => {
        const user = getUser(socket.id)
        io.to(user.room).emit('locationMessage', generateLocationMessage(user.username, `https://google.com/maps?q=${coords.latitude},${coords.longitude}`))
        callback()
    })

    socket.on('disconnect', () => {
        const user = removeUser(socket.id)

        if (user) {
            io.to(user.room).emit('message', generateMessage('Admin', `${user.username} has left!`))
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: getUsersInRoom(user.room)
            })
        }
    })
})

/*================================================================
                    Path Declarations
=================================================================*/
const viewpath = path.join(__dirname, "./public/views");
const partialpath = path.join(__dirname, "./public/views/partials");
app.set("view engine", "hbs");
app.set("views", viewpath);
hbs.registerPartials(partialpath);

/* ========================================================== 
            Internal App Modules/Packages Required
============================================================ */
var Admin = require('./server/victim.js');                          //Exchange routes & DB Queries
var Mentor = require('./server/volunteer.js');						//Exchange routes & DB Queries 
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())
/* ========================================================== 
                     Middleware
============================================================ */
app.use(logger('dev')); 	//log every request to the console


/* ========================================================== 
            parsing through the incoming data
============================================================ */
// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, './public')))

// initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(cookieParser());


/* ========================================================== 
                    ROUTES - for Express
============================================================ */

// // route for victim's dashboard
// victim(app);

// // route for volunteer's dashboard
// volunteer(app);

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));


// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});

// middleware function to check for logged-in users
// {issues to b fixed here!!}
var sessionChecker = (req, res, next) => {
    if (req.session.user && req.cookies.user_sid) {
        if (this.role === "Admin") {
            res.redirect('/index', {
                Admin: this.username
            })
            console.log("inside Admin");
        } else if (this.role === "Mentor"){
            res.redirect('/teacher',{
                teacher: this.username
            })
            console.log("Inside Mentor")
        }
    } else {
        next();
    }    
};


// route for Home-Page
app.get('/', sessionChecker, (req, res) => {
    res.redirect('/login');
});


// route for user signup
app.route('/signup')
    .get(sessionChecker, (req, res) => {
        res.render("signup");
    })
    .post((req, res) => {
        User.create({
            username: req.body.username,
            role: req.body.role,
            email: req.body.email,
            password: req.body.password
        })
        .then(async (user) => {
            req.session.user = user.dataValues;
            res.redirect('/dashboard');
        })
        .catch(error => {
            res.render('signup');
        });
    });


// route for user Login
app.route('/login')
    .get(sessionChecker, (req, res) => {
        res.render('login')
    })
    .post((req, res) => {
        var username = req.body.username,
            password = req.body.password;
        console.log('Hello')
        User.findOne({ where: { username: username } }).then(async function (user) {
            if (!user) {
                res.redirect('/login');
            } else if (!await user.validPassword(password)) {
                res.redirect('/login');
            } else {
                req.session.user = user.dataValues;
                res.redirect('/dashboard');
            }
        });
    });

// route for user logout
app.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/');
    } else {
        res.redirect('/login');
    }
});


// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});






/* ========================================================== 
                    Other Stupid Pages
============================================================ */
app.get('/victim' ,(req, res) => {
    res.render("7-sem",{
        title: "7-sem assessment Page"
    })
})

app.get('/volunteer' ,(req, res) => {
    res.render("8-sem",{
        title: "8-sem assessment Page!"
    })
})

app.get('final', (req ,res) => {
    res.render("final",{
        title: "Final assessment Page"
    })
})



/* ========================================================== 
                    Listening port
============================================================ */
var server = http.createServer(app);
server.listen(3000, () => {
    console.log('Server is up on port 3000')
})