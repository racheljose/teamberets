// const socketio = require('socket.io')

// const server = http.createServer(app)
// const io = socketio(server)

// const publicDirectoryPath = path.join(__dirname, '../public')

// app.use(express.static(publicDirectoryPath))

// io.on('connection', (socket) => {
//     socket.on('sendLocation', (coords, callback) => {
//         const user = getUser(socket.id)
//         io.to(user.room).emit('locationMessage', generateLocationMessage(user.username, `https://google.com/maps?q=${coords.latitude},${coords.longitude}`))
//         callback()
//     })
// })

const $sendLocationButton = document.querySelector('#send-location')

$sendLocationButton.addEventListener('click', () => {
    if (!navigator.geolocation) {
        return alert('Geolocation is not supported by your browser.')
    }

    $sendLocationButton.setAttribute('disabled', 'disabled')

    navigator.geolocation.getCurrentPosition((position) => {
            var latitude = position.coords.latitude
            console.log(latitude)
            var longitude = position.coords.longitude
            console.log(longitude)
        })
    console.log(latitude)
    console.log(longitude)
    // var userlocation= function(latitude,longitude)
    // {
    //     this.latitude= latitude;
    //     this.longitude = longitude;
    // }
    // module.exports = userlocation;
})